<?php
include "connection.php";

$username_pattern = "/^[a-zA-Z0-9]{4,20}$/";
$email_pattern = "/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/";
$password_pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/";

$username = $_POST["name"];
$email = $_POST["email"];
$password = $_POST["password"];
$confirm_password = $_POST["confirm_password"];

if (!preg_match($username_pattern, $username)){
    echo '<script>alert("Invalid Username!!!")</script>';
} else if(!preg_match($email_pattern, $email)){
    echo '<script>alert("Invalid Email Format!!!")</script>';
} else if(!preg_match($password_pattern,$password)){
    echo '<script>alert("Invalid Password Format!!!")</script>';
} else if($password != $confirm_password){
    echo '<script>alert("Password doesnot matches!!!")</script>';
} else if(mysqli_num_rows(mysqli_query($conn, "SELECT * FROM User where Username='$username'")) > 0){     
    echo '<script>alert("Username Already Taken!!!")</script>';
} else if(mysqli_num_rows(mysqli_query($conn, "SELECT * FROM User where Email='$email'")) > 0){
    echo '<script>alert("Email Already Taken!!!")</script>';
} else {
    $h_password = password_hash($password, PASSWORD_DEFAULT);
    $insert_query = "INSERT INTO User(Username, Email, Password,Role) VALUES ('$username', '$email', '$h_password','Admin')";
    if(mysqli_query($conn, $insert_query)){
        echo '<script>alert("Inserted the data!!!")</script>';
    } else{
        echo "Error: " . $query . "<br>" . mysqli_error($conn);
    }
}

?>